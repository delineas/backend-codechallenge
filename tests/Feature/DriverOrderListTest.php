<?php

namespace Tests\Feature;

use App\Driver;
use App\Order;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DriverOrderListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function drivers_can_get_its_orders_list_on_a_date()
    {
        $this->withoutExceptionHandling();

        $driver = factory(Driver::class)->create(); // only one driver, all orders assigned to him

        $deliveryDate = Carbon::createFromFormat('d/m/Y', '04/05/2018');
        factory(Order::class, 10)->create(); // other deliverys
        $firstOrder = factory(Order::class)->create(['delivery_date' => $deliveryDate, 'delivery_time_start' => '17:00', 'delivery_time_end' => '19:00']);
        $secondOrder = factory(Order::class)->create(['delivery_date' => $deliveryDate, 'delivery_time_start' => '10:00', 'delivery_time_end' => '11:00']);
        $thirdOrder = factory(Order::class)->create(['delivery_date' => $deliveryDate, 'delivery_time_start' => '14:00', 'delivery_time_end' => '20:00']);

        $response = $this->json('GET', "/drivers/{$driver->id}/orders?date={$deliveryDate->format('d/m/Y')}");

        $response->assertExactJson([
            [
                'firstname' => $firstOrder->client->firstname,
                'lastname' => $firstOrder->client->lastname,
                'phone' => $firstOrder->client->phone,
                'address' => $firstOrder->address,
                'delivery_date' => $deliveryDate->format('d/m/Y'),
                'delivery_time_start' => '17:00',
                'delivery_time_end' => '19:00',
            ],
            [
                'firstname' => $secondOrder->client->firstname,
                'lastname' => $secondOrder->client->lastname,
                'phone' => $secondOrder->client->phone,
                'address' => $secondOrder->address,
                'delivery_date' => $deliveryDate->format('d/m/Y'),
                'delivery_time_start' => '10:00',
                'delivery_time_end' => '11:00',
            ],
            [
                'firstname' => $thirdOrder->client->firstname,
                'lastname' => $thirdOrder->client->lastname,
                'phone' => $thirdOrder->client->phone,
                'address' => $thirdOrder->address,
                'delivery_date' => $deliveryDate->format('d/m/Y'),
                'delivery_time_start' => '14:00',
                'delivery_time_end' => '20:00',
            ],
        ]);
    }

    /** @test */
    public function drivers_cant_get_its_orders_list_without_a_date()
    {
        $driver = factory(Driver::class)->create();

        $response = $this->json('GET', "/drivers/{$driver->id}/orders");

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['date']);
    }

    /** @test */
    public function drivers_cant_get_its_orders_list_with_wrong_date_format()
    {
        $driver = factory(Driver::class)->create();

        $response = $this->json('GET', "/drivers/{$driver->id}/orders?date=36/89/44444");

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['date']);
    }
}
