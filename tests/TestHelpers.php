<?php

namespace Tests;

trait TestHelpers {
    protected function getDefaultOrderRequestFields(array $attributes = []): array
    {
        return array_merge([
            'firstname' => 'Darth',
            'lastname' => 'Vader',
            'email' => 'darthvader@empire.com',
            'phone' => '45612378',
            'address' => 'Tatooine 24, 2ºB',
            'delivery_date' => '04/05/2018',
            'delivery_time_start' => '09:00',
            'delivery_time_end' => '12:00',
        ], $attributes);
    }
}