<?php

use App\Client;
use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'client_id' => function() {
            return factory(Client::class)->create();
        },
        'address' => $faker->address,
        'delivery_date' => $faker->date(),
        'delivery_time_start' => '10:00',
        'delivery_time_end' => '12:00',
    ];
});
