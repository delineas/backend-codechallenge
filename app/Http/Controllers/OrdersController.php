<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\OrderRequest;
use Carbon\Carbon;

class OrdersController extends Controller
{
    public function store(OrderRequest $request)
    {
        $client = Client::updateOrCreate(
            ['email' => $request->email],
            [
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'phone' => $request->phone,
            ]
        );

        $order = $client->orders()->create([
            'client_id' => $client->id,
            'address' => $request->address,
            'delivery_date' => Carbon::createFromFormat('d/m/Y', $request->delivery_date),
            'delivery_time_start' => $request->delivery_time_start,
            'delivery_time_end' => $request->delivery_time_end,
        ]);

        return $order;
    }
}
