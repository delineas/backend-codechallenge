<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Resources\DriverOrderResource;
use Illuminate\Http\Request;

class DriverOrdersController extends Controller
{
    public function __invoke(Driver $driver, Request $request)
    {
        $request->validate(['date' => 'required|date_format:d/m/Y']);

        return DriverOrderResource::collection(
            $driver->orders()->inDate($request->date)->orderByTimeRange()->get()
        );
    }
}
