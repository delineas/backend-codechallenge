<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request): array
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
            'delivery_date' => 'required|date|date_format:d/m/Y',
            'delivery_time_start' => 'required|date_format:H:i',
            'delivery_time_end' => [
                'required',
                'date_format:H:i',
                'after:delivery_time_start',
                function ($attribute, $value, $fail) use ($request) {
                    if ($this->isMoreTimeThanMax($request->delivery_time_start, $value)) {
                        return $fail("Delivery range is more than allowed (8h)");
                    }
                }
            ],
        ];
    }

    private function isMoreTimeThanMax($timeStart, $timeEnd): bool
    {
        if (!strtotime($timeStart) || !strtotime($timeEnd)) { // wrong format
            return false;
        }

        $timeInterval = date_diff(date_create($timeStart), date_create($timeEnd));

        return $timeInterval->h > config('constants.max_timerange') || ($timeInterval->h === config('constants.max_timerange') && ($timeInterval->i || !$timeInterval->s));
    }
}
