<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/orders', 'OrdersController@store')->name('orders.store');

Route::get('/drivers/{driver}/orders', 'DriverOrdersController')->name('driver.orders.index');